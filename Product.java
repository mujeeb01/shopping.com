package shoppingonline;

public class Product {
String name;
int id;
String brand;
long qty;
double price;
public Product( int id,String name,  String brand, int qty, double price) {
	this.name = name;
	this.id = id;
	this.brand = brand;
	this.qty = qty;
	this.price = price;
}
@Override
public String toString() {
	return "Product [name=" + name + ", id=" + id + ", brand=" + brand + ", qty=" + qty + ", price="
			+ price + "]";
}
}

