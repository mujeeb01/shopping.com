package shoppingonline;


public class Customer {
String name;
Long phone;
String email;
cart customercart;
public Customer(String name,String email, Long phone ) {
	
	this.name = name;
	this.phone = phone;
	this.email = email;
	this.customercart = new cart();
}
public void login() {
	System.out.println("Hello "+this.name+" Welcome to Shopping.com\n--------------------------");
	
}
public void addcart(int item){
    if(Inventory.productlist.get(item-1).qty<1) System.out.println("Sorry! Item out of stock.");
    else{
        Inventory.productlist.get(item-1).qty--;
        System.out.println(Inventory.productlist.get(item-1).name+ " successfully added to cart.");
        customercart.total+=Inventory.productlist.get(item-1).price;
        if(customercart.cartlist.containsKey(Inventory.productlist.get(item-1).name))
        		customercart.cartlist.put(Inventory.productlist.get(item-1).name,
        		customercart.cartlist.get(Inventory.productlist.get(item-1).name) + 1);
        else customercart.cartlist.put(Inventory.productlist.get(item-1).name, 1);
        customercart.details();
}
}
}
