package shoppingonline;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  Scanner sc=new Scanner(System.in);
	        Customer c1=new Customer("Mujeeb", "Mujeebtarry@gmail.com", 9622605363L);
	        Product p1=new Product(1,"Laptop", "HP", 4, 50000);
	        Product p2=new Product(2,"Watch", "Fastrack", 3, 2000);
	        Product p3=new Product(3,"TShirt", "Dolce & Gabbana", 5, 1000);
	        Product p4=new Product(4,"Head Phone", "Boat", 10, 800);
	        Product p5=new Product(5,"Notebook", "Classmate", 9, 50);
	        
	        Inventory.productlist.add(p1);
	        Inventory.productlist.add(p2);
	        Inventory.productlist.add(p3);
	        Inventory.productlist.add(p4);
	        Inventory.productlist.add(p5);

	        c1.login();

	        while(true){
	            for(Product p:Inventory.productlist){
	                System.out.println(p);
	            }
	            System.out.println("Poduct you want to buy?");
	            int choice=sc.nextInt();
	            c1.addcart(choice);
	            System.out.println("Do you want to continue(yes/no)?");
	            String c=sc.next();
	            if(c.toLowerCase().equals("no")) 
	            	System.exit(0);
	        }
	}

}
